FROM python:3.7-alpine

WORKDIR /desafio-sogo

RUN apk update

ADD . /desafio-sogo

RUN pip install -r requirements.txt

EXPOSE 5000

ENV FLASK_ENV=production

CMD flask run -h 0.0.0.0 -p 5000

