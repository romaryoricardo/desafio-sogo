import os
import pytest
from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True

    with app.test_client() as client:
        with app.app_context():
            yield client

def test_root(client):
    """Start with a blank database."""

    rv = client.get('/')
    assert b'Desafio SOGO' in rv.data